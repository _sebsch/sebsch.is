---
title: "About sebsch"
date:  2020-05-10

---

I'm a software engikneer from Berlin Germany.


With a strong profession to opensource software and linux I love to write good and sustainable backend code in python, go and rust. Also working on fully automated infrastructure projects, including kubernetes, ansible and docker, via quality ensuring CI/CD environments is also a big part of my toolset.

If you have any questions,  just write me via e-mail.

