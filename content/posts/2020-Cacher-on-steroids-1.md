--- 
title: "recapilutation of the cacher pattern"
date:  2020-04-26
tags: ["design pattern", "rust", "closure"]
categories: ["rust", "cacher-on-steroids"]
draft: true
---


---
**NOTE**

This Article is part of a (planed) series of posts about extending the 
[Cacher-Closure in the Rustbook](https://doc.rust-lang.org/book/ch13-01-closures.html)

---