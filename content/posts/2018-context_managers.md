---
title: "Git clone and delete after using"
date: "2018-08-19"
tags: ["design pattern", "context-manager", "python"]
categories: ["python"] 
---



Automatic changes in  a git-repository can cause merge-conflicts. Therefore it can be handy to temporary clone the remote, do your changes and delete the repo after the changes are pushed. The following code does this with the help of a context-manager.

```python
    from git import Repo
    from shutil import rmtree
    
    class GitRepositoryContextManager(object):
        def __init__(self, remote, file_path, branch='master'):
            self.file_path = file_path
            
            self.repo = Repo.clone_from(remote, self.file_path, branch=branch)
    
        def __enter__(self):
            return self.repo
    
        def __exit__(self, exc_type, exc_val, exc_tb):
            rmtree(self.file_path)
```
